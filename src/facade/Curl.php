<?php
namespace TpLxp\facade;
use think\Facade;

/**
 * 静态代理
 * @mixin \TpLxp\Curl
 */
class Curl extends Facade
{
    protected static function getFacadeClass()
    {
        return \TpLxp\Curl::class;
    }
}